/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package program;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author franc
 */
public class FrmCaja extends javax.swing.JFrame {

    /**
     * Creates new form FrmCaja
     */
    public FrmCaja() {
        initComponents();
        Radiobotones.add(RBtnTarjeta);
        Radiobotones.add(RbtnEfectivo);
    }
    
    void Encender() {
        if (jToggleEncender.isSelected()) {
            jComboBoxPeliculas.setEnabled(true);
            jComboBoxHora.setEnabled(true);
            AgregarAComboboxPelicula();

            jToggleEncender.setText("Apagar");
        } else {
            jComboBoxPeliculas.setEnabled(false);
            jComboBoxHora.setEnabled(false);
            limpiar();
            Desactivados();
            jToggleEncender.setText("Encender");
        }
    }
    void Desactivados() {
        jComboBoxPeliculas.setEnabled(false);
        jComboBoxHora.setEnabled(false);
        RbtnEfectivo.setEnabled(false);
        RBtnTarjeta.setEnabled(false);
        jtextBolestosAdultos.setEnabled(false);
        jtextBolestosNiños.setEnabled(false);
        btnContinuar.setEnabled(false);
    }
    
    
     void Activar() {
        RbtnEfectivo.setEnabled(true);
        RBtnTarjeta.setEnabled(true);
        jtextBolestosAdultos.setEnabled(true);
        jtextBolestosNiños.setEnabled(true);
        btnContinuar.setEnabled(true);
    }
     
     
     
    void AgregarAComboboxPelicula() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        modelo.addElement("Seleccione");
        modelo.addElement("Dead Pool 2");
        modelo.addElement("End Game");
        modelo.addElement("Intensamente");
        modelo.addElement("monsters university");
        modelo.addElement("Sonic");

        jComboBoxPeliculas.setModel(modelo);

    }
    
    
     void seleccionPelicula() {
        int combo;
        combo = jComboBoxPeliculas.getSelectedIndex();
        if (combo == 1 || combo == 2 || combo == 3 || combo == 4 || combo == 5) {
            Activar();
            MenoresEdad();
        } else {
            RbtnEfectivo.setEnabled(false);
            RBtnTarjeta.setEnabled(false);
            jtextBolestosAdultos.setEnabled(false);
            jtextBolestosNiños.setEnabled(false);
            btnContinuar.setEnabled(false);
        }
    }
    
     
     void caratulas() {
        int combo;
        combo = jComboBoxPeliculas.getSelectedIndex();
        switch (combo) {
            case 0: {
                ImageIcon i = new ImageIcon("");
                lbImagen.setIcon(i);
                break;
            }
            case 1: {
                LimCantidad();
                Preview.jLabelSala.setText("4");
                ImageIcon i = new ImageIcon("src/Cartelera/deadpool.jpeg");
                lbImagen.setIcon(i);
                DefaultComboBoxModel modelo = new DefaultComboBoxModel();
                modelo.addElement("Seleccione");
                modelo.addElement("4:00 pm");
                modelo.addElement("6:00 pm");
                modelo.addElement("8:00 pm");
                jComboBoxHora.setModel(modelo);
                break;
            }
            case 2: {
                LimCantidad();
                Preview.jLabelSala.setText("2");
                ImageIcon i = new ImageIcon("src/Cartelera/end_game.jpg");
                lbImagen.setIcon(i);
                DefaultComboBoxModel modelo = new DefaultComboBoxModel();
                modelo.addElement("Seleccione");
                modelo.addElement("5:00 pm");
                modelo.addElement("7:00 pm");
                modelo.addElement("9:00 pm");
                jComboBoxHora.setModel(modelo);
                break;
            }
            case 3: {
                LimCantidad();
                Preview.jLabelSala.setText("3");
                ImageIcon i = new ImageIcon("src/Cartelera/intensa.jpeg");
                lbImagen.setIcon(i);
                DefaultComboBoxModel modelo = new DefaultComboBoxModel();
                modelo.addElement("Seleccione");
                modelo.addElement("12:00 pm");
                modelo.addElement("2:00 pm");
                modelo.addElement("4:00 pm");
                jComboBoxHora.setModel(modelo);
                break;
            }
            case 4: {
                LimCantidad();
                Preview.jLabelSala.setText("5");
                ImageIcon i = new ImageIcon("src/Cartelera/monster.jpg");
                lbImagen.setIcon(i);
                DefaultComboBoxModel modelo = new DefaultComboBoxModel();
                modelo.addElement("Seleccione");
                modelo.addElement("1:00 pm");
                modelo.addElement("3:00 pm");
                modelo.addElement("6:30 pm");
                jComboBoxHora.setModel(modelo);
                break;
            }
            case 5: {
                LimCantidad();
                Preview.jLabelSala.setText("1");
                ImageIcon i = new ImageIcon("src/Cartelera/sonic.jpg");
                lbImagen.setIcon(i);
                DefaultComboBoxModel modelo = new DefaultComboBoxModel();
                modelo.addElement("Seleccione");
                modelo.addElement("2:00 pm");
                modelo.addElement("5:00 pm");
                modelo.addElement("8:00 pm");
                jComboBoxHora.setModel(modelo);
                break;
            }
            default:
                break;
        }
    }
     
     
     
     
     
     
    
     
      
      
      void LimCantidad() {
        jtextBolestosAdultos.setText("");
        jtextBolestosNiños.setText("");
    }
      
      
       void limpiar() {
        ImageIcon i = new ImageIcon("");
        lbImagen.setIcon(i);
        jtextBolestosAdultos.setText("");
        Radiobotones.clearSelection();
        jComboBoxPeliculas.removeAllItems();
        jComboBoxHora.removeAllItems();
    }
      
      
       void calculo() {

        double cantidadAdultos = 0.0, cantidadNiños = 0.0;
        double precioAdultos = 0.0, precioNiños = 0.0;
        double TotalN = 0, TotalA = 0, Total;

        if ("".equals(jtextBolestosNiños.getText())) {
            String ninguno = "0.0";
            Preview.jLabelTotalNiños.setText(ninguno);
            Preview.jTextFieldCantidadDeBoletosNiños.setText("0");
        } else {
            cantidadNiños = Double.parseDouble(jtextBolestosNiños.getText());
            precioNiños = Double.parseDouble(lblPrecioNiño.getText());
            TotalN = (cantidadNiños * precioNiños);
            Preview.jLabelTotalNiños.setText(TotalN + "");
        }

        if ("".equals(jtextBolestosAdultos.getText())) {
            jtextBolestosAdultos.setText("");
        } else {

            cantidadAdultos = Double.parseDouble(jtextBolestosAdultos.getText());
            precioAdultos = Double.parseDouble(lblPrecioAdulto.getText());
            TotalA = (cantidadAdultos * precioAdultos);
            Preview.jLabelTotalAdultos.setText(TotalA + "");
        }
        Total = TotalA + TotalN;
        Preview.jLabelTotalPago.setText(Total + "");

    }
       
       
       
       
       void pasaDatos() {
        Preview.jTextFieldCantidadDeBoletosAdultos.setText(jtextBolestosAdultos.getText());
        Preview.jTextFieldCantidadDeBoletosNiños.setText(jtextBolestosNiños.getText());
    }
       
       
       
       
      
    
      
      void MenoresEdad() {
        int combo = jComboBoxPeliculas.getSelectedIndex();
        if (combo == 1 || combo == 2) {
            lblMenores.setVisible(true);
            jtextBolestosNiños.setVisible(false);
            lblMenores.setText("No apta para menores de edad");
        }
        if (combo == 3 || combo == 4 || combo == 5) {
            jtextBolestosNiños.setVisible(true);
            lblMenores.setVisible(false);
        }
    }
      
      void tarjeta(){
        if (RBtnTarjeta.isSelected()) {
            Preview.jTextFieldEfectivoRecibido.setText("Paga con tarjeta");
            Preview.jTextFieldEfectivoRecibido.setEditable(false);
            Preview.jLabel14.setVisible(false);
            Preview.jButton3.setVisible(false);
            Preview.jButton1.setEnabled(true);
        }
    }
      

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        Radiobotones = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jComboBoxPeliculas = new javax.swing.JComboBox<>();
        jToggleEncender = new javax.swing.JToggleButton();
        jComboBoxHora = new javax.swing.JComboBox<>();
        lblPeliculas = new javax.swing.JLabel();
        lblHora = new javax.swing.JLabel();
        lbImagen = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        lblPrecioAdulto = new javax.swing.JLabel();
        lblsigno1 = new javax.swing.JLabel();
        lblPrecio_adulto = new javax.swing.JLabel();
        lblPrecio_niño = new javax.swing.JLabel();
        lblsigno2 = new javax.swing.JLabel();
        lblPrecioNiño = new javax.swing.JLabel();
        lblBoletos = new javax.swing.JLabel();
        lblAdultos_boletos = new javax.swing.JLabel();
        lblNiños_bolestos = new javax.swing.JLabel();
        lblCantidad = new javax.swing.JLabel();
        lblPago = new javax.swing.JLabel();
        lblMenores = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        RbtnEfectivo = new javax.swing.JRadioButton();
        RBtnTarjeta = new javax.swing.JRadioButton();
        jtextBolestosAdultos = new javax.swing.JTextField();
        jtextBolestosNiños = new javax.swing.JTextField();
        btnContinuar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        MnuAdminstrador = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(96, 36));
        setPreferredSize(new java.awt.Dimension(845, 433));

        jPanel1.setLayout(new java.awt.BorderLayout());

        jLabel1.setText("Bienvenido a Caja");
        jPanel2.add(jLabel1);

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jComboBoxPeliculas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBoxPeliculas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPeliculasActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jComboBoxPeliculas, gridBagConstraints);

        jToggleEncender.setText("Encender");
        jToggleEncender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleEncenderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel3.add(jToggleEncender, gridBagConstraints);

        jComboBoxHora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel3.add(jComboBoxHora, gridBagConstraints);

        lblPeliculas.setText("Peliculas");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel3.add(lblPeliculas, gridBagConstraints);

        lblHora.setText("Hora");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel3.add(lblHora, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(45, 10, 45, 10);
        jPanel3.add(lbImagen, gridBagConstraints);

        jPanel5.setPreferredSize(new java.awt.Dimension(540, 267));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        lblPrecioAdulto.setText("120");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel5.add(lblPrecioAdulto, gridBagConstraints);

        lblsigno1.setText("C$");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel5.add(lblsigno1, gridBagConstraints);

        lblPrecio_adulto.setText("Precios Adultos");
        jPanel5.add(lblPrecio_adulto, new java.awt.GridBagConstraints());

        lblPrecio_niño.setText("Precio niño");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel5.add(lblPrecio_niño, gridBagConstraints);

        lblsigno2.setText("C$");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanel5.add(lblsigno2, gridBagConstraints);

        lblPrecioNiño.setText("80");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel5.add(lblPrecioNiño, gridBagConstraints);

        lblBoletos.setText("Boletos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 1;
        jPanel5.add(lblBoletos, gridBagConstraints);

        lblAdultos_boletos.setText("Adultos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 2;
        jPanel5.add(lblAdultos_boletos, gridBagConstraints);

        lblNiños_bolestos.setText("  Niños  ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 2;
        jPanel5.add(lblNiños_bolestos, gridBagConstraints);

        lblCantidad.setText("Cantidad");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 4;
        jPanel5.add(lblCantidad, gridBagConstraints);

        lblPago.setText("Forma de pago");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel5.add(lblPago, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel5.add(lblMenores, gridBagConstraints);

        jPanel4.setLayout(new java.awt.BorderLayout());

        RbtnEfectivo.setText("Efectivo");
        jPanel4.add(RbtnEfectivo, java.awt.BorderLayout.CENTER);

        RBtnTarjeta.setText("tarjeta");
        jPanel4.add(RBtnTarjeta, java.awt.BorderLayout.PAGE_END);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        jPanel5.add(jPanel4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel5.add(jtextBolestosAdultos, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel5.add(jtextBolestosNiños, gridBagConstraints);

        btnContinuar.setText("Continuar");
        btnContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContinuarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        jPanel5.add(btnContinuar, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanel3.add(jPanel5, gridBagConstraints);

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jMenu1.setText("Seleccionar");

        jMenuItem1.setText("Menu Principal");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        MnuAdminstrador.setText("Administrador");
        MnuAdminstrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnuAdminstradorActionPerformed(evt);
            }
        });
        jMenu1.add(MnuAdminstrador);

        jMenuItem3.setText("Salir");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(742, 429));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void MnuAdminstradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnuAdminstradorActionPerformed
        DlgContraseña obj = new DlgContraseña(this,true);
            obj.setVisible(true);
    }//GEN-LAST:event_MnuAdminstradorActionPerformed

    private void jToggleEncenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleEncenderActionPerformed
        Encender();
    }//GEN-LAST:event_jToggleEncenderActionPerformed

    private void jComboBoxPeliculasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPeliculasActionPerformed
        seleccionPelicula();
        caratulas();
    }//GEN-LAST:event_jComboBoxPeliculasActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        FrmPrincipalInicio obj = new FrmPrincipalInicio();
            obj.setVisible(true);
            this.dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void btnContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContinuarActionPerformed
        if (jComboBoxPeliculas.getSelectedIndex() == 0 || jComboBoxHora.getSelectedIndex() == 0 || jtextBolestosAdultos.getText().length() == 0) {
            JOptionPane.showMessageDialog(this, "Todavía hacen falta datos por completar", "Complete datos", JOptionPane.WARNING_MESSAGE);
        } else {
            Preview F = new Preview();
            pasaDatos();
            Preview.jTextFieldPelicula.setText(jComboBoxPeliculas.getSelectedItem().toString());
            Preview.jTextFieldHora.setText(jComboBoxHora.getSelectedItem().toString());
            tarjeta();
            calculo();
            F.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btnContinuarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmCaja().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem MnuAdminstrador;
    private javax.swing.JRadioButton RBtnTarjeta;
    private javax.swing.ButtonGroup Radiobotones;
    private javax.swing.JRadioButton RbtnEfectivo;
    private javax.swing.JButton btnContinuar;
    private javax.swing.JComboBox<String> jComboBoxHora;
    private javax.swing.JComboBox<String> jComboBoxPeliculas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JToggleButton jToggleEncender;
    private javax.swing.JTextField jtextBolestosAdultos;
    private javax.swing.JTextField jtextBolestosNiños;
    private javax.swing.JLabel lbImagen;
    private javax.swing.JLabel lblAdultos_boletos;
    private javax.swing.JLabel lblBoletos;
    private javax.swing.JLabel lblCantidad;
    private javax.swing.JLabel lblHora;
    private javax.swing.JLabel lblMenores;
    private javax.swing.JLabel lblNiños_bolestos;
    private javax.swing.JLabel lblPago;
    private javax.swing.JLabel lblPeliculas;
    private javax.swing.JLabel lblPrecioAdulto;
    private javax.swing.JLabel lblPrecioNiño;
    private javax.swing.JLabel lblPrecio_adulto;
    private javax.swing.JLabel lblPrecio_niño;
    private javax.swing.JLabel lblsigno1;
    private javax.swing.JLabel lblsigno2;
    // End of variables declaration//GEN-END:variables
}
